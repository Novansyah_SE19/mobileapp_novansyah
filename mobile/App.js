import * as React from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import io from 'socket.io-client';

export default class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      chatMessage: '',
      Messages: []
    }
  }

  componentDidMount() {
    this.socket = io('http://192.168.169.195:3000')
    this.socket.on('chat message', msg => {
      this.setState({Messages:  [...this.state.Messages, msg]})
    })
  }

  submitChatMessage = () => {
    this.socket.emit('chat message', this.state.chatMessage);
    this.setState({chatMessage: ''})
  }

  render() {
    const Messages = this.state.Messages.map(chatMessage => (
      <Text style={{marginLeft: 10}} key={chatMessage}>{chatMessage}</Text>
    ))
    return (
      <View style={styles.container}>
        <TextInput 
          style={{width: '100%', height: 40, borderWidth: 1}}
          placeholder='Type message here'
          autoCorrect={false}
          value={this.state.chatMessage}
          onSubmitEditing={this.submitChatMessage}
          onChangeText={chatMessage => {
            this.setState({chatMessage});
          }}
        />
        {Messages}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})